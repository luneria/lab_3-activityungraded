public class Application{
	public static void main(String[]args)
	{
		Student student1 = new Student();
		student1.name = "Luneria";
		student1.origin = "Ireland";
		student1.age = 21;
		System.out.println(student1.name);
		System.out.println(student1.origin);
		//System.out.println(student1.age);
		student1.AmNotThatOld();
		
		Student student2 = new Student();
		student2.name = "Veronica";
		student2.origin = "Spain";
		student2.age = 18;
		System.out.println(student2.name);
		System.out.println(student2.origin);
		//System.out.println(student2.age);
		student2.AmNotThatOld();
		
		Student[] section3 = new Student[3];
		section3[0] = student1;
        section3[1] = student2;
		
		//System.out.println(section3[0].name);
		//System.out.println(section3[2].name);
		
		section3[2] = new Student();
		section3[2].name ="Sammy";
		section3[2].origin = "Mexico";
		section3[2].age = 30;
		System.out.println(section3[2].name);
		System.out.println(section3[2].origin);
		System.out.println(section3[2].age);
	}
}